//
//  testWriter.cpp
//  racing
//
//  Created by Jung on 4/18/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "catch.hpp"
#include "ResultWriter.h"

using namespace racing;

TEST_CASE("writer empty", "[Test writer]") {
    ResultWriter writer;

    REQUIRE(writer.GetJson() == "{}");
}


TEST_CASE("writer data", "[Test writer]") {
    ResultWriter writer;

    writer.SendThrottle(1.0f);
    writer.RecvData("{\"hi\":\"hello\"}");

    REQUIRE(writer.GetJson() == "{\"race\":[{\"throttle\":1,\"data\":{\"hi\":\"hello\"}}]}");
}


TEST_CASE("writer track data", "[Test writer]") {
    ResultWriter writer;

    writer.SetTrack("{\"track\":\"hi\"}");

    writer.SendThrottle(1.0f);
    writer.RecvData("{\"hi\":\"hello\"}");

    REQUIRE(writer.GetJson() == "{\"track\":{\"track\":\"hi\"},\"race\":[{\"throttle\":1,\"data\":{\"hi\":\"hello\"}}]}");
}


TEST_CASE("writer data2", "[Test writer]") {
    ResultWriter writer;

    writer.SendThrottle(0.5f);

    writer.SendThrottle(1.0f);
    writer.RecvData("{\"hi\":\"hello\"}");

    REQUIRE(writer.GetJson() == "{\"race\":[{\"throttle\":0.5},{\"throttle\":1,\"data\":{\"hi\":\"hello\"}}]}");
}

TEST_CASE("writer data3", "[Test writer]") {
    ResultWriter writer;

    writer.SendThrottle(1.0f);
    writer.RecvData("{\"hi\":\"hello\"}");

    writer.RecvData("{\"hi\":\"hello2\"}");

    REQUIRE(writer.GetJson() == "{\"race\":[{\"throttle\":1,\"data\":{\"hi\":\"hello\"}}]}");
}

TEST_CASE("writer data4", "[Test writer]") {
    ResultWriter writer;

    writer.SetTrack("{\"track\":\"hi\"}");

    writer.SendThrottle(1.0f);
    writer.RecvData("{\"hi\":\"hello\"}");

    writer.SendThrottle(0.5f);

    REQUIRE(writer.GetJson() == "{\"track\":{\"track\":\"hi\"},\"race\":[{\"throttle\":1,\"data\":{\"hi\":\"hello\"}},{\"throttle\":0.5}]}");
}
