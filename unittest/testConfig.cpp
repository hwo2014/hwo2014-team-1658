//
//  testConfig.cpp
//  racing
//
//  Created by mimu on 4/15/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "catch.hpp"
#include "ConfigUtils.h"

#include <string>
#include <vector>

using namespace racing;

TEST_CASE("parse", "[Config test]") {
    std::vector<std::string> args {"./bot", "123.123.123.123", "8080"};
    {
        auto config = ConfigUtils::ParseArgs(args);

        REQUIRE(!config);
    }

    args.push_back("botname");
    args.push_back("botkey");

    {
        auto config = ConfigUtils::ParseArgs(args);

        REQUIRE(config);
        REQUIRE(config->host == "123.123.123.123");
        REQUIRE(config->port == "8080");
        REQUIRE(config->botname == "botname");
        REQUIRE(config->botkey == "botkey");
        REQUIRE(config->trackName.empty());
        REQUIRE(config->password.empty());
    }

    args.push_back("trackName");
    args.push_back("password");

    {
        auto config = ConfigUtils::ParseArgs(args);

        REQUIRE(config);
        REQUIRE(config->host == "123.123.123.123");
        REQUIRE(config->port == "8080");
        REQUIRE(config->botname == "botname");
        REQUIRE(config->botkey == "botkey");
        REQUIRE(config->trackName == "trackName");
        REQUIRE(config->password == "password");
    }

    args.push_back("create");
    args.push_back("2");

    {
        auto config = ConfigUtils::ParseArgs(args);

        REQUIRE(config);
        REQUIRE(config->host == "123.123.123.123");
        REQUIRE(config->port == "8080");
        REQUIRE(config->botname == "botname");
        REQUIRE(config->botkey == "botkey");
        REQUIRE(config->trackName == "trackName");
        REQUIRE(config->password == "password");
        REQUIRE(config->testType == "create");
        REQUIRE(config->carCount == 2);
    }
}