//
//  testData.cpp
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "catch.hpp"
#include "Datas.h"
#include "FileUtils.h"
#include "DataUtils.h"

using namespace racing;

TEST_CASE("join", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("join.json");
    REQUIRE(!fileContent.empty());

    JoinData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.name == "Schumacher");
    REQUIRE(data.key == "UEWJBVNHDS");

    REQUIRE(data.ToJson() == "{\"msgType\":\"join\",\"data\":{\"name\":\"Schumacher\",\"key\":\"UEWJBVNHDS\"}}");
}

TEST_CASE("yourCar", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("yourCar.json");
    REQUIRE(!fileContent.empty());

    YourCarData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.name == "Schumacher");
    REQUIRE(data.color == "red");

    REQUIRE(data.ToJson() == "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"Schumacher\",\"color\":\"red\"}}");
}

TEST_CASE("gameInit", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("gameInit.json");
    REQUIRE(!fileContent.empty());

    GameInitData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.race.track.id == "indianapolis");
    REQUIRE(data.race.track.name == "Indianapolis");
    REQUIRE(data.race.track.pieces.size() == 3);
    REQUIRE(data.race.track.pieces[0].length == 100);
    REQUIRE(data.race.track.pieces[1].length == 100);
    REQUIRE(data.race.track.pieces[1].isSwitch == true);
    REQUIRE(data.race.track.pieces[2].radius == 200);
    REQUIRE(data.race.track.pieces[2].angle == 22.5f);

    REQUIRE(data.race.track.lanes.size() == 3);
    REQUIRE(data.race.track.lanes[0].distanceFromCenter == -20);
    REQUIRE(data.race.track.lanes[0].index == 0);
    REQUIRE(data.race.track.lanes[1].distanceFromCenter == 0);
    REQUIRE(data.race.track.lanes[1].index == 1);
    REQUIRE(data.race.track.lanes[2].distanceFromCenter == 20);
    REQUIRE(data.race.track.lanes[2].index == 2);

    REQUIRE(data.race.track.startingPoint.x == -340);
    REQUIRE(data.race.track.startingPoint.y == -96);
    REQUIRE(data.race.track.startingPoint.angle == 90);

    REQUIRE(data.race.cars.size() == 2);

    REQUIRE(data.race.cars[0].name == "Schumacher");
    REQUIRE(data.race.cars[0].color == "red");
    REQUIRE(data.race.cars[0].length == 40);
    REQUIRE(data.race.cars[0].width == 20);
    REQUIRE(data.race.cars[0].guideFlagPosition == 10);

    REQUIRE(data.race.cars[1].name == "Rosberg");
    REQUIRE(data.race.cars[1].color == "blue");
    REQUIRE(data.race.cars[1].length == 40);
    REQUIRE(data.race.cars[1].width == 20);
    REQUIRE(data.race.cars[1].guideFlagPosition == 10);

    REQUIRE(data.race.raceSession.laps == 3);
    REQUIRE(data.race.raceSession.maxLapTimeMs == 30000);
    REQUIRE(data.race.raceSession.quickRace == true);
}

TEST_CASE("carPosition", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("carPositions.json");
    REQUIRE(!fileContent.empty());

    CarPositionData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.positions.size() == 2);
    REQUIRE(data.gameId == "OIUHGERJWEOI");
    REQUIRE(data.gameTick == 0);

    REQUIRE(data.positions[0].name == "Schumacher");
    REQUIRE(data.positions[0].color == "red");
    REQUIRE(data.positions[0].angle == 0);
    REQUIRE(data.positions[0].pieceIndex == 0);
    REQUIRE(data.positions[0].inPieceDistance == 0);
    REQUIRE(data.positions[0].lap == 0);
    REQUIRE(data.positions[0].startLaneIndex == 0);
    REQUIRE(data.positions[0].endLaneIndex == 0);

    REQUIRE(data.positions[1].name == "Rosberg");
    REQUIRE(data.positions[1].color == "blue");
    REQUIRE(data.positions[1].angle == 45);
    REQUIRE(data.positions[1].pieceIndex == 0);
    REQUIRE(data.positions[1].inPieceDistance == 20);
    REQUIRE(data.positions[1].lap == 0);
    REQUIRE(data.positions[1].startLaneIndex == 1);
    REQUIRE(data.positions[1].endLaneIndex == 1);
}

TEST_CASE("throttle", "[Data test]") {
    ThrottleData data;
    data.data = 0.5;
    REQUIRE(data.ToJson() == "{\"msgType\":\"throttle\",\"data\":0.5}");

    data.gameTick = 1;
    REQUIRE(data.ToJson() == "{\"msgType\":\"throttle\",\"data\":0.5,\"gameTick\":1}");
}

TEST_CASE("gameEnd", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("gameEnd.json");
    REQUIRE(!fileContent.empty());

    GameEndData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.results.size() == 2);
    REQUIRE(data.bestLaps.size() == 2);

    REQUIRE(data.results[0].name == "Schumacher");
    REQUIRE(data.results[0].color == "red");
    REQUIRE(data.results[0].laps == 3);
    REQUIRE(data.results[0].ticks == 9999);
    REQUIRE(data.results[0].millis == 45245);

    REQUIRE(data.results[1].name == "Rosberg");
    REQUIRE(data.results[1].color == "blue");
    REQUIRE(data.results[1].laps == 0);
    REQUIRE(data.results[1].ticks == 0);
    REQUIRE(data.results[1].millis == 0);
}

TEST_CASE("crash", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("crash.json");
    REQUIRE(!fileContent.empty());

    CrashData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.name == "Rosberg");
    REQUIRE(data.color == "blue");
    REQUIRE(data.gameId == "OIUHGERJWEOI");
    REQUIRE(data.gameTick == 3);
}

TEST_CASE("spawn", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("spawn.json");
    REQUIRE(!fileContent.empty());

    SpawnData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.name == "Rosberg");
    REQUIRE(data.color == "blue");
    REQUIRE(data.gameId == "OIUHGERJWEOI");
    REQUIRE(data.gameTick == 150);
}

TEST_CASE("lapFinished", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("lapFinished.json");
    REQUIRE(!fileContent.empty());

    LapFinishedData data;
    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.name == "Schumacher");
    REQUIRE(data.color == "red");

    REQUIRE(data.lapTime.lap == 1);
    REQUIRE(data.lapTime.ticks == 666);
    REQUIRE(data.lapTime.millis == 6660);

    REQUIRE(data.raceTime.lap == 1);
    REQUIRE(data.raceTime.ticks == 666);
    REQUIRE(data.raceTime.millis == 6660);

    REQUIRE(data.overall == 1);
    REQUIRE(data.fastestLap == 1);

    REQUIRE(data.gameId == "OIUHGERJWEOI");
    REQUIRE(data.gameTick == 300);
}

TEST_CASE("DNF", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("dnf.json");
    REQUIRE(!fileContent.empty());

    DNFData data;

    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.name == "Rosberg");
    REQUIRE(data.color == "blue");

    REQUIRE(data.reason == "disconnected");

    REQUIRE(data.gameId == "OIUHGERJWEOI");
    REQUIRE(data.gameTick == 650);
}

TEST_CASE("Finish", "[Data test]") {
    auto fileContent = FileUtils::LoadFile("finish.json");
    REQUIRE(!fileContent.empty());

    FinishData data;

    REQUIRE(data.FromJson(fileContent));

    REQUIRE(data.name == "Schumacher");
    REQUIRE(data.color == "red");

    REQUIRE(data.gameId == "OIUHGERJWEOI");
    REQUIRE(data.gameTick == 2345);
}

TEST_CASE("swtichLane", "[Data test]") {
    SwitchLaneData data;
    data.data = "left";
    REQUIRE(data.ToJson() == "{\"msgType\":\"switchLane\",\"data\":\"left\"}");
}

TEST_CASE("createRace", "[Data test]") {
    CreateRaceData data;
    data.name = "Poookie";
    data.key = "TZv5Gju82ApEFw";
    data.trackName = "keimola";
    data.password = "password!";
    data.carCount = 1;

    REQUIRE(data.ToJson() == "{\"msgType\":\"createRace\",\"data\":{\"botId\":{\"name\":\"Poookie\",\"key\":\"TZv5Gju82ApEFw\"},\"trackName\":\"keimola\",\"password\":\"password!\",\"carCount\":1}}");
}

TEST_CASE("joinRace", "[Data test]") {
    JoinRaceData data;
    data.name = "name";
    data.key = "abc";
    data.trackName = "track";
    data.password = "password";
    data.carCount = 3;

    REQUIRE(data.ToJson() == "{\"msgType\":\"joinRace\",\"data\":{\"botId\":{\"name\":\"name\",\"key\":\"abc\"},\"trackName\":\"track\",\"password\":\"password\",\"carCount\":3}}");
}
