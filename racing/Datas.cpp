//
//  Datas.cpp
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "Datas.h"
#include <sstream>
#include <cassert>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace rapidjson;

namespace racing {
//------------------------------------------------------------------------------
#pragma mark - JoinData
//------------------------------------------------------------------------------
std::string JoinData::ToJson() const
{
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);

    writer.StartObject();

    writer.String("msgType");
    writer.String("join");

    writer.String("data");
    {
        writer.StartObject();
        writer.String("name");
        writer.String(name.c_str());

        writer.String("key");
        writer.String(key.c_str());
        writer.EndObject();
    }

    writer.EndObject();

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool JoinData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember("data") || !doc["data"].IsObject()) {
        return false;
    }

    auto const& data = doc["data"];

    name = data["name"].GetString();
    key = data["key"].GetString();

    return true;
}
//------------------------------------------------------------------------------
#pragma mark - YourCarData
//------------------------------------------------------------------------------
std::string YourCarData::ToJson() const
{
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);

    writer.StartObject();

    writer.String("msgType");
    writer.String("yourCar");

    writer.String("data");
    {
        writer.StartObject();
        writer.String("name");
        writer.String(name.c_str());

        writer.String("color");
        writer.String(color.c_str());
        writer.EndObject();
    }

    writer.EndObject();

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool YourCarData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember("data") || !doc["data"].IsObject()) {
        return false;
    }
    
    auto const& data = doc["data"];

    if (!data.HasMember("name") || !data["name"].IsString()
        || !data.HasMember("color") || !data["color"].IsString()) {
        return false;
    }
    
    name = data["name"].GetString();
    color = data["color"].GetString();
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - GameInit data
//------------------------------------------------------------------------------
std::string GameInitData::ToJson() const
{
    StringBuffer buf;
    ///@todo Not implemented
    assert(false);

//    Writer<StringBuffer> writer(buf);
//
//    writer.StartObject();
//
//    writer.String("msgType");
//    writer.String("gameInit");
//
//    writer.String("data");
//    writer.StartObject();
//
//    writer.String("race");
//    {
//        writer.StartObject();
//        writer.String("track");
//        {
//            writer.StartObject();
//
//            writer.String("id");
//            writer.String(race.track.id.c_str());
//
//            writer.String("name");
//            writer.String(race.track.name.c_str());
//
//            writer.String("pieces");
//            writer.StartArray();
//            for (auto const& piece : race.track.pieces) {
//
//            }
//            writer.EndArray();
//
//            writer.EndObject();
//        }
//
//
//
//        writer.EndObject();
//    }
//    
//    writer.EndObject();
//    writer.EndObject();

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool GameInitData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember("data") || !doc["data"].IsObject()
        || !doc["data"].HasMember("race") || !doc["data"]["race"].IsObject()) {
        return false;
    }

    auto const& raceData = doc["data"]["race"];

    if (!raceData.HasMember("track") || !raceData["track"].IsObject()
        || !raceData.HasMember("cars") || !raceData["cars"].IsArray()
        || !raceData.HasMember("raceSession") || !raceData["raceSession"].IsObject()) {
        return false;
    }
    
    {
        ///@note track
        auto const& trackData = raceData["track"];
        if (!trackData.HasMember("id") || !trackData["id"].IsString()
            || !trackData.HasMember("name") || !trackData["name"].IsString()
            || !trackData.HasMember("pieces") || !trackData["pieces"].IsArray()
            || !trackData.HasMember("lanes") || !trackData["lanes"].IsArray()
            || !trackData.HasMember("startingPoint") || !trackData["startingPoint"].IsObject()) {
            return false;
        }

        race.track.id = trackData["id"].GetString();
        race.track.name = trackData["name"].GetString();

        ///@note pieces
        for (SizeType i=0; i<trackData["pieces"].Size(); ++i) {
            Race::Track::Piece piece;
            auto const& pieceData = trackData["pieces"][i];
            if (pieceData.HasMember("length")) {
                if (pieceData["length"].IsDouble()) {
                    piece.length = pieceData["length"].GetDouble();
                } else if (pieceData["length"].IsUint()) {
                    piece.length = pieceData["length"].GetUint();
                }
            }

            if (pieceData.HasMember("switch") && pieceData["switch"].IsBool()) {
                piece.isSwitch = pieceData["switch"].GetBool();
            }
            if (pieceData.HasMember("radius")) {
                if (pieceData["radius"].IsDouble()) {
                    piece.radius = pieceData["radius"].GetDouble();
                } else if (pieceData["radius"].IsUint()) {
                    piece.radius = pieceData["radius"].GetUint();
                }
            }

            if (pieceData.HasMember("angle") && pieceData["angle"].IsDouble()) {
                piece.angle = pieceData["angle"].GetDouble();
            }
            race.track.pieces.push_back(piece);
        }

        ///@note lanes
        for (SizeType i=0; i<trackData["lanes"].Size(); ++i) {
            Race::Track::Lane lane;
            auto const& laneData = trackData["lanes"][i];
            if (!laneData.HasMember("distanceFromCenter") || !laneData["distanceFromCenter"].IsInt()
                || !laneData.HasMember("index") || !laneData["index"].IsUint()) {
                continue;
            }
            lane.distanceFromCenter = laneData["distanceFromCenter"].GetInt();
            lane.index = laneData["index"].GetUint();

            race.track.lanes.push_back(lane);
        }

        ///@note startingPoint
        auto const& spData = trackData["startingPoint"];
        if (!spData.HasMember("position") || !spData["position"].IsObject()
            || !spData["position"].HasMember("x") || !spData["position"]["x"].IsDouble()
            || !spData["position"].HasMember("y") || !spData["position"]["y"].IsDouble()
            || !spData.HasMember("angle") || !spData["angle"].IsDouble()) {
            return false;
        }

        race.track.startingPoint.x = spData["position"]["x"].GetDouble();
        race.track.startingPoint.y = spData["position"]["y"].GetDouble();
        race.track.startingPoint.angle = spData["angle"].GetDouble();
    }

    {
        ///@note cars
        for (SizeType i=0; i<raceData["cars"].Size(); ++i) {
            auto const& carData = raceData["cars"][i];
            if (!carData.HasMember("id") || !carData["id"].IsObject()
                || !carData["id"].HasMember("name") || !carData["id"]["name"].IsString()
                || !carData["id"].HasMember("color") || !carData["id"]["color"].IsString()
                || !carData.HasMember("dimensions") || !carData["dimensions"].IsObject()
                || !carData["dimensions"].HasMember("length") || !carData["dimensions"]["length"].IsDouble()
                || !carData["dimensions"].HasMember("width") || !carData["dimensions"]["width"].IsDouble()
                || !carData["dimensions"].HasMember("guideFlagPosition") || !carData["dimensions"]["guideFlagPosition"].IsDouble()) {
                return false;
            }
            Car car;
            car.name = carData["id"]["name"].GetString();
            car.color = carData["id"]["color"].GetString();
            car.length = carData["dimensions"]["length"].GetDouble();
            car.width = carData["dimensions"]["width"].GetDouble();
            car.guideFlagPosition = carData["dimensions"]["guideFlagPosition"].GetDouble();

            race.cars.push_back(car);

        }
    }

    {
        ///@note race session
        auto const& rsData = raceData["raceSession"];
        if (!rsData.HasMember("laps") || !rsData["laps"].IsUint()
            || !rsData.HasMember("maxLapTimeMs") || !rsData["maxLapTimeMs"].IsUint()
            || !rsData.HasMember("quickRace") || !rsData["quickRace"].IsBool()) {
            return false;
        }
        race.raceSession.laps = rsData["laps"].GetUint();
        race.raceSession.maxLapTimeMs = rsData["maxLapTimeMs"].GetUint();
        race.raceSession.quickRace = rsData["quickRace"].GetBool();
    }
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - CarPositionData
//------------------------------------------------------------------------------
CarPositionData::CarPositionData()
: gameTick(0)
{

}
//------------------------------------------------------------------------------
std::string CarPositionData::ToJson() const
{
    StringBuffer buf;
    ///@todo Not implemented
    assert(false);

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool CarPositionData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsArray()
        || !doc.HasMember("gameId") || !doc["gameId"].IsString()
        ) {
        return false;
    }

    gameId = doc["gameId"].GetString();
    if (doc.HasMember("gameTick") && doc["gameTick"].IsUint()) {
        gameTick = doc["gameTick"].GetUint();
    }

    auto const& data = doc["data"];

    for (SizeType i=0; i<data.Size(); ++i) {
        auto const& carData = data[i];

        if (!carData.HasMember("id") || !carData["id"].IsObject()
            || !carData["id"].HasMember("name") || !carData["id"]["name"].IsString()
            || !carData["id"].HasMember("color") || !carData["id"]["color"].IsString()
            || !carData.HasMember("angle") || !carData["angle"].IsDouble()
            || !carData.HasMember("piecePosition") || !carData["piecePosition"].IsObject()
            || !carData["piecePosition"].HasMember("pieceIndex") || !carData["piecePosition"]["pieceIndex"].IsUint()
            || !carData["piecePosition"].HasMember("inPieceDistance") || !carData["piecePosition"]["inPieceDistance"].IsDouble()
            || !carData["piecePosition"].HasMember("lap") || !carData["piecePosition"]["lap"].IsUint()
            || !carData["piecePosition"].HasMember("lane") || !carData["piecePosition"]["lane"].IsObject()
            || !carData["piecePosition"]["lane"].HasMember("startLaneIndex") || !carData["piecePosition"]["lane"]["startLaneIndex"].IsUint()
            || !carData["piecePosition"]["lane"].HasMember("endLaneIndex") || !carData["piecePosition"]["lane"]["endLaneIndex"].IsUint()
            ) {
            return false;
        }

        CarPosition position;
        position.name = carData["id"]["name"].GetString();
        position.color = carData["id"]["color"].GetString();

        position.angle = carData["angle"].GetDouble();
        position.pieceIndex = carData["piecePosition"]["pieceIndex"].GetUint();
        position.inPieceDistance = carData["piecePosition"]["inPieceDistance"].GetDouble();
        position.startLaneIndex = carData["piecePosition"]["lane"]["startLaneIndex"].GetUint();
        position.endLaneIndex = carData["piecePosition"]["lane"]["endLaneIndex"].GetUint();
        position.lap = carData["piecePosition"]["lap"].GetUint();

        positions.push_back(position);
    }

    return true;
}
//------------------------------------------------------------------------------
#pragma mark - Throttle data
//------------------------------------------------------------------------------
ThrottleData::ThrottleData()
: data(0.0f)
, gameTick(0)
{}
//------------------------------------------------------------------------------
std::string ThrottleData::ToJson() const
{
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);

    writer.StartObject();

    writer.String("msgType");
    writer.String("throttle");

    writer.String("data");
    writer.Double(data);

    if (gameTick != 0) {
        writer.String("gameTick");
        writer.Uint(gameTick);
    }

    writer.EndObject();

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool ThrottleData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember("data") || !doc["data"].IsDouble()) {
        return false;
    }

    data = doc["data"].GetDouble();
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - GameEnd data
//------------------------------------------------------------------------------
std::string GameEndData::ToJson() const
{
    StringBuffer buf;
    ///@todo Not implemented
    assert(false);

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool GameEndData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsObject()
        || !doc["data"].HasMember("results") || !doc["data"]["results"].IsArray()
        || !doc["data"].HasMember("bestLaps") || !doc["data"]["bestLaps"].IsArray()) {
        return false;
    }

    for (SizeType i=0; i<doc["data"]["results"].Size(); ++i) {
        auto const& resultData = doc["data"]["results"][i];
        if (!resultData.HasMember("car") || !resultData["car"].IsObject()
            || !resultData["car"].HasMember("name") || !resultData["car"]["name"].IsString()
            || !resultData["car"].HasMember("color") || !resultData["car"]["color"].IsString()) {
            return false;
        }
        Result result;

        result.name = resultData["car"]["name"].GetString();
        result.color = resultData["car"]["color"].GetString();

        if (resultData.HasMember("result") && resultData["result"].IsObject()
            && resultData["result"].HasMember("laps") && resultData["result"]["laps"].IsUint()
            && resultData["result"].HasMember("ticks") && resultData["result"]["ticks"].IsUint()
            && resultData["result"].HasMember("millis") && resultData["result"]["millis"].IsUint()) {

            result.laps = resultData["result"]["laps"].GetUint();
            result.ticks = resultData["result"]["ticks"].GetUint();
            result.millis = resultData["result"]["millis"].GetUint();
        }

        results.push_back(result);
    }

    for (SizeType i=0; i<doc["data"]["bestLaps"].Size(); ++i) {
        auto const& resultData = doc["data"]["bestLaps"][i];

        if (!resultData.HasMember("car") || !resultData["car"].IsObject()
            || !resultData["car"].HasMember("name") || !resultData["car"]["name"].IsString()
            || !resultData["car"].HasMember("color") || !resultData["car"]["color"].IsString()) {
            return false;
        }
        Result result;

        result.name = resultData["car"]["name"].GetString();
        result.color = resultData["car"]["color"].GetString();

        if (resultData.HasMember("result") && resultData["result"].IsObject()
            && resultData["result"].HasMember("laps") && resultData["result"]["laps"].IsUint()
            && resultData["result"].HasMember("ticks") && resultData["result"]["ticks"].IsUint()
            && resultData["result"].HasMember("millis") && resultData["result"]["millis"].IsUint64()) {

            result.laps = resultData["result"]["laps"].GetUint();
            result.ticks = resultData["result"]["ticks"].GetUint();
            result.millis = resultData["result"]["millis"].GetUint64();
        }

        bestLaps.push_back(result);
    }

    return true;
}
//------------------------------------------------------------------------------
#pragma mark - Crash data
//------------------------------------------------------------------------------
CrashData::CrashData()
: gameTick(0)
{}
//------------------------------------------------------------------------------
std::string CrashData::ToJson() const
{
    ///@todo Not implemented
    assert(false);
    StringBuffer buf;

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool CrashData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsObject()
        || !doc["data"].HasMember("name") || !doc["data"]["name"].IsString()
        || !doc["data"].HasMember("color") || !doc["data"]["color"].IsString()
        || !doc.HasMember("gameId") || !doc["gameId"].IsString()
        || !doc.HasMember("gameTick") || !doc["gameTick"].IsUint()) {
        return false;
    }
    
    name = doc["data"]["name"].GetString();
    color = doc["data"]["color"].GetString();
    gameId = doc["gameId"].GetString();
    gameTick = doc["gameTick"].GetUint();

    return true;
}
//------------------------------------------------------------------------------
#pragma mark - Spawn data
//------------------------------------------------------------------------------
SpawnData::SpawnData()
: gameTick(0)
{}
//------------------------------------------------------------------------------
std::string SpawnData::ToJson() const
{
    ///@todo Not implemented
    assert(false);
    StringBuffer buf;

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool SpawnData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsObject()
        || !doc["data"].HasMember("name") || !doc["data"]["name"].IsString()
        || !doc["data"].HasMember("color") || !doc["data"]["color"].IsString()
        || !doc.HasMember("gameId") || !doc["gameId"].IsString()
        || !doc.HasMember("gameTick") || !doc["gameTick"].IsUint()) {
        return false;
    }

    name = doc["data"]["name"].GetString();
    color = doc["data"]["color"].GetString();
    gameId = doc["gameId"].GetString();
    gameTick = doc["gameTick"].GetUint();
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - LapFinishedData
//------------------------------------------------------------------------------
LapFinishedData::LapFinishedData()
: overall(0)
, fastestLap(0)
, gameTick(0)
{}
//------------------------------------------------------------------------------
std::string LapFinishedData::ToJson() const
{
    ///@todo Not implemented
    assert(false);
    StringBuffer buf;

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool LapFinishedData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsObject()
        || !doc.HasMember("gameId") || !doc["gameId"].IsString()
        || !doc.HasMember("gameTick") || !doc["gameTick"].IsUint()) {
        return false;
    }

    gameId = doc["gameId"].GetString();
    gameTick = doc["gameTick"].GetUint();

    if (!doc["data"].HasMember("car") || !doc["data"]["car"].IsObject()
        || !doc["data"]["car"].HasMember("name") || !doc["data"]["car"]["name"].IsString()
        || !doc["data"]["car"].HasMember("color") || !doc["data"]["car"]["color"].IsString()) {
        return false;
    }

    name = doc["data"]["car"]["name"].GetString();
    color = doc["data"]["car"]["color"].GetString();

    if (!doc["data"].HasMember("ranking") || !doc["data"]["ranking"].IsObject()
        || !doc["data"]["ranking"].HasMember("overall") || !doc["data"]["ranking"]["overall"].IsUint()
        || !doc["data"]["ranking"].HasMember("fastestLap") || !doc["data"]["ranking"]["fastestLap"].IsUint()) {
        return false;
    }

    overall = doc["data"]["ranking"]["overall"].GetUint();
    fastestLap = doc["data"]["ranking"]["fastestLap"].GetUint();


    if (!doc["data"].HasMember("lapTime") || !doc["data"]["lapTime"].IsObject()
        || !doc["data"]["lapTime"].HasMember("lap") || !doc["data"]["lapTime"]["lap"].IsUint()
        || !doc["data"]["lapTime"].HasMember("ticks") || !doc["data"]["lapTime"]["ticks"].IsUint()
        || !doc["data"]["lapTime"].HasMember("millis") || !doc["data"]["lapTime"]["millis"].IsUint64()) {
        return false;
    }

    lapTime.lap = doc["data"]["lapTime"]["lap"].GetUint();
    lapTime.ticks = doc["data"]["lapTime"]["ticks"].GetUint();
    lapTime.millis = doc["data"]["lapTime"]["millis"].GetUint64();

    if (!doc["data"].HasMember("raceTime") || !doc["data"]["raceTime"].IsObject()
        || !doc["data"]["raceTime"].HasMember("laps") || !doc["data"]["raceTime"]["laps"].IsUint()
        || !doc["data"]["raceTime"].HasMember("ticks") || !doc["data"]["raceTime"]["ticks"].IsUint()
        || !doc["data"]["raceTime"].HasMember("millis") || !doc["data"]["raceTime"]["millis"].IsUint64()) {
        return false;
    }

    raceTime.lap = doc["data"]["raceTime"]["laps"].GetUint();
    raceTime.ticks = doc["data"]["raceTime"]["ticks"].GetUint();
    raceTime.millis = doc["data"]["raceTime"]["millis"].GetUint64();
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - DNF data
//------------------------------------------------------------------------------
DNFData::DNFData()
: gameTick(0)
{}
//------------------------------------------------------------------------------
std::string DNFData::ToJson() const
{
    ///@todo Not implemented
    assert(false);
    StringBuffer buf;

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool DNFData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsObject()
        || !doc["data"].HasMember("car") || !doc["data"]["car"].IsObject()
        || !doc["data"]["car"].HasMember("name") || !doc["data"]["car"]["name"].IsString()
        || !doc["data"]["car"].HasMember("color") || !doc["data"]["car"]["color"].IsString()
        || !doc["data"].HasMember("reason") || !doc["data"]["reason"].IsString()
        || !doc.HasMember("gameId") || !doc["gameId"].IsString()
        || !doc.HasMember("gameTick") || !doc["gameTick"].IsUint()) {
        return false;
    }

    name = doc["data"]["car"]["name"].GetString();
    color = doc["data"]["car"]["color"].GetString();
    reason = doc["data"]["reason"].GetString();
    gameId = doc["gameId"].GetString();
    gameTick = doc["gameTick"].GetUint();
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - Finish data
//------------------------------------------------------------------------------
FinishData::FinishData()
: gameTick(0)
{}
//------------------------------------------------------------------------------
std::string FinishData::ToJson() const
{
    ///@todo Not implemented
    assert(false);
    StringBuffer buf;

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool FinishData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsObject()
        || !doc["data"].HasMember("name") || !doc["data"]["name"].IsString()
        || !doc["data"].HasMember("color") || !doc["data"]["color"].IsString()
        || !doc.HasMember("gameId") || !doc["gameId"].IsString()
        || !doc.HasMember("gameTick") || !doc["gameTick"].IsUint()) {
        return false;
    }

    name = doc["data"]["name"].GetString();
    color = doc["data"]["color"].GetString();
    gameId = doc["gameId"].GetString();
    gameTick = doc["gameTick"].GetUint();
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - Finish data
//------------------------------------------------------------------------------
std::string SwitchLaneData::ToJson() const
{
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);

    writer.StartObject();

    writer.String("msgType");
    writer.String("switchLane");

    writer.String("data");
    writer.String(data.c_str());

    writer.EndObject();
    
    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool SwitchLaneData::FromJson(const std::string &json)
{
    Document doc;
    doc.Parse<0>(json.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("data") || !doc["data"].IsString()) {
        return false;
    }

    data = doc["data"].GetString();
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - CreateRaceData
//------------------------------------------------------------------------------
CreateRaceData::CreateRaceData()
: carCount(0)
{}
//------------------------------------------------------------------------------
std::string CreateRaceData::ToJson() const
{
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);

    writer.StartObject();

    writer.String("msgType");
    writer.String("createRace");

    writer.String("data");
    {
        writer.StartObject();
        {
            writer.String("botId");
            writer.StartObject();

            writer.String("name");
            writer.String(name.c_str());
            writer.String("key");
            writer.String(key.c_str());

            writer.EndObject();
        }

        writer.String("trackName");
        writer.String(trackName.c_str());

        writer.String("password");
        writer.String(password.c_str());

        writer.String("carCount");
        writer.Uint(carCount);

        writer.EndObject();
    }

    writer.EndObject();

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool CreateRaceData::FromJson(const std::string &json)
{
    ///@note not implemented
    assert(false);
    
    return true;
}
//------------------------------------------------------------------------------
#pragma mark - JoinRaceData
//------------------------------------------------------------------------------
JoinRaceData::JoinRaceData()
: carCount(0)
{}
//------------------------------------------------------------------------------
std::string JoinRaceData::ToJson() const
{
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);

    writer.StartObject();

    writer.String("msgType");
    writer.String("joinRace");

    writer.String("data");
    {
        writer.StartObject();
        {
            writer.String("botId");
            writer.StartObject();

            writer.String("name");
            writer.String(name.c_str());
            writer.String("key");
            writer.String(key.c_str());

            writer.EndObject();
        }

        writer.String("trackName");
        writer.String(trackName.c_str());

        writer.String("password");
        writer.String(password.c_str());

        writer.String("carCount");
        writer.Uint(carCount);

        writer.EndObject();
    }

    writer.EndObject();

    return std::move(buf.GetString());
}
//------------------------------------------------------------------------------
bool JoinRaceData::FromJson(const std::string &json)
{
    ///@note not implemented
    assert(false);
    
    return true;
}
//------------------------------------------------------------------------------
}