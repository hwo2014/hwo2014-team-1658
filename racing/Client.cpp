//
//  Client.cpp
//  racing
//
//  Created by mimu on 4/15/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "Client.h"
#include <sstream>
#include "ConfigUtils.h"

using boost::asio::ip::tcp;


namespace racing {
//------------------------------------------------------------------------------
Client::Client(boost::asio::io_service& io_service,
               boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
               GameConfig const& config)
: io_service_(io_service)
, socket_(io_service)
, gameLogic_(config, [this](std::string && msg){
    this->Write(std::move(msg));
})
{
    buffer_.prepare(8192);

    DoConnect(endpoint_iterator);
}
//------------------------------------------------------------------------------
void Client::Write(std::string && msg)
{
    if (msg.find("\n") == std::string::npos) {
        msg.append("\n");
    }

    io_service_.post(
                     [this, msg]()
                     {
                         bool write_in_progress = !sendQueue_.empty();
                         sendQueue_.push_back(msg);
                         if (!write_in_progress)
                         {
                             DoWrite();
                         }
                     });
}
//------------------------------------------------------------------------------
void Client::Close()
{
    io_service_.post([this]() { socket_.close(); });
}
//------------------------------------------------------------------------------
void Client::DoConnect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
{
    boost::asio::async_connect(socket_, endpoint_iterator,
                               [this](boost::system::error_code ec, tcp::resolver::iterator)
                               {
                                   if (!ec) {
                                       gameLogic_.OnConnect();
                                       DoRead();
                                   }
                               });
}
//------------------------------------------------------------------------------
void Client::DoRead()
{
    boost::asio::async_read_until(socket_,
                            buffer_,
                            "\n",
                            [this](boost::system::error_code ec, std::size_t length)
                            {
                                if (!ec && length > 0) {
                                    std::stringstream ss;
                                    ss << &buffer_;

                                    std::string recvString = ss.str();
                                    std::string delimiter = "\n";

                                    std::size_t start = 0U;
                                    auto end = recvString.find(delimiter);

                                    while (end != std::string::npos) {
                                        std::string data = recvString.substr(start, end - start);
                                        gameLogic_.OnRead(std::move(data));

                                        start = end + delimiter.length();
                                        end = recvString.find(delimiter, start);
                                    }

//                                    gameLogic_.OnRead(std::move(ss.str()));

                                    DoRead();
                                } else {
                                    socket_.close();
                                }
                            });
}
//------------------------------------------------------------------------------
void Client::DoWrite()
{
    boost::asio::async_write(socket_,
                             boost::asio::buffer(sendQueue_.front().data(),
                                                 sendQueue_.front().length()),
                             [this](boost::system::error_code ec, std::size_t /*length*/)
                             {
                                 if (!ec)
                                 {
                                     sendQueue_.pop_front();
                                     if (!sendQueue_.empty())
                                     {
                                         DoWrite();
                                     }
                                 }
                                 else
                                 {
                                     socket_.close();
                                 }
                             });
}
//------------------------------------------------------------------------------
}