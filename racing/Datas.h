//
//  Datas.h
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__Datas__
#define __racing__Datas__

#include <string>
#include <vector>

namespace racing {
//------------------------------------------------------------------------------
#pragma mark - Raw data
//------------------------------------------------------------------------------
struct Car
{
    Car()
    : length(0.0f)
    , width(0.0f)
    , guideFlagPosition(0.0f)
    {}
    std::string name;
    std::string color;
    float length;
    float width;
    float guideFlagPosition;
};
//------------------------------------------------------------------------------
struct CarPosition
{
    CarPosition()
    : angle(0.0f)
    , inPieceDistance(0.0f)
    {}
    std::string name;
    std::string color;
    float angle;
    float inPieceDistance;

    std::uint32_t pieceIndex;
    std::uint32_t startLaneIndex;
    std::uint32_t endLaneIndex;

    std::uint16_t lap;
};
//------------------------------------------------------------------------------
struct Race
{
    struct Track
    {
        struct Piece
        {
            Piece()
            : isSwitch(false)
            , length(0.0f)
            , radius(0.0f)
            , angle(0.0f)
            {}
            bool isSwitch;
            float length;
            float radius;
            float angle;
        };

        struct Lane
        {
            Lane()
            : distanceFromCenter(0.0f)
            , index(0)
            {}

            float distanceFromCenter;
            std::uint32_t index;
        };

        struct StartingPoint
        {
            StartingPoint()
            : x(0.0f)
            , y(0.0f)
            , angle(0.0f)
            {}
            float x;
            float y;
            float angle;
        };

        std::string id;
        std::string name;
        std::vector<Piece> pieces;
        std::vector<Lane> lanes;
        StartingPoint startingPoint;
    };

    struct RaceSession
    {
        RaceSession()
        : laps(0)
        , maxLapTimeMs(0)
        , quickRace(false)
        {}
        std::uint16_t laps;
        std::uint64_t maxLapTimeMs;
        bool quickRace;
    };

    Track track;
    std::vector<Car> cars;
    RaceSession raceSession;
};
//------------------------------------------------------------------------------
struct Result
{
    Result()
    : laps(0)
    , ticks(0)
    , millis(0)
    {}
    std::string name;
    std::string color;
    std::uint16_t laps;
    std::uint32_t ticks;
    std::uint64_t millis;
};
//------------------------------------------------------------------------------
struct Lap
{
    Lap()
    : lap(0)
    , ticks(0)
    , millis(0)
    {}
    std::uint16_t lap;
    std::uint32_t ticks;
    std::uint64_t millis;
};
//------------------------------------------------------------------------------
#pragma mark - JSON data
//------------------------------------------------------------------------------
enum class DataTypes
{
    Invalid,
    Error,

    Join,
    YourCar,
    GameInit,
    CarPosition,
    GameStart,
    GameEnd,
    Throttle,
    TournamentEnd,
    Crash,
    Spawn,
    LapFinished,
    DNF,
    Finish,
    SwitchLane,

    CreateRace,
    JoinRace,

    Ping
};
//------------------------------------------------------------------------------
class Data
{
public:
    virtual std::string ToJson() const = 0;
    virtual bool FromJson(std::string const& json) = 0;
};
//------------------------------------------------------------------------------
class JoinData : public Data
{
public:
    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string key;
};
//------------------------------------------------------------------------------
class YourCarData : public Data
{
public:
    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string color;
};
//------------------------------------------------------------------------------
class GameInitData : public Data
{
public:
    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    Race race;
};
//------------------------------------------------------------------------------
class CarPositionData : public Data
{
public:
    CarPositionData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::vector<CarPosition> positions;
    std::string gameId;
    std::uint32_t gameTick;
};
//------------------------------------------------------------------------------
class ThrottleData : public Data
{
public:
    ThrottleData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    float data;
    std::uint32_t gameTick;
};
//------------------------------------------------------------------------------
class GameEndData : public Data
{
public:
    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;
    
public:
    std::vector<Result> results;
    std::vector<Result> bestLaps;
};
//------------------------------------------------------------------------------
class CrashData : public Data
{
public:
    CrashData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string color;
    std::string gameId;
    std::uint32_t gameTick;
};
//------------------------------------------------------------------------------
class SpawnData : public Data
{
public:
    SpawnData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string color;
    std::string gameId;
    std::uint32_t gameTick;
};
//------------------------------------------------------------------------------
class LapFinishedData : public Data
{
public:
    LapFinishedData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string color;
    std::string gameId;

    std::uint32_t overall;
    std::uint32_t fastestLap;

    std::uint32_t gameTick;

    Lap lapTime;
    Lap raceTime;
};
//------------------------------------------------------------------------------
class DNFData : public Data
{
public:
    DNFData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string color;
    std::string reason;

    std::string gameId;
    std::uint32_t gameTick;
};
//------------------------------------------------------------------------------
class FinishData : public Data
{
public:
    FinishData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string color;

    std::string gameId;
    std::uint32_t gameTick;
};
//------------------------------------------------------------------------------
class SwitchLaneData : public Data
{
public:
    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string data;
};
//------------------------------------------------------------------------------
class CreateRaceData : public Data
{
public:
    CreateRaceData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string key;
    std::string trackName;
    std::string password;
    std::uint16_t carCount;
};
//------------------------------------------------------------------------------
class JoinRaceData : public Data
{
public:
    JoinRaceData();

    std::string ToJson() const override;
    bool FromJson(std::string const& json) override;

public:
    std::string name;
    std::string key;
    std::string trackName;
    std::string password;
    std::uint16_t carCount;
};
//------------------------------------------------------------------------------
} // racing

#endif /* defined(__racing__Datas__) */
