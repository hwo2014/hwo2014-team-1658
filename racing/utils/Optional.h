//
//  Optional.h
//
//  Created by mimu on 2014/02/18.
//
//

#ifndef racing_Optional_h
#define racing_Optional_h

#include "RaceAssert.h"

namespace racing {

template <typename T>
class optional final
{
private:
	T data;
	bool valid;

public:
	constexpr optional()
		: valid(false)
	{}
	
	optional(optional const&) = default;
	optional(optional &&) = default;
	
	constexpr optional(T const& v)
		: data(v)
		, valid(true)
	{}
	
	constexpr optional(T && v)
		: data(std::move(v))
		, valid(true)
	{}
	
	~optional() = default;
	
	optional & operator=(optional const&) = default;
	optional & operator=(optional &&) = default;

	optional & operator=(T const& v)
	{
		this->valid = true;
		this->data = v;
		return *this;
	}
	
	optional & operator=(T && v)
	{
		this->valid = true;
		this->data = std::move(v);
		return *this;
	}
	
	constexpr T const* operator->() const noexcept
	{
		return &data;
	}
	
	T* operator->() noexcept
	{
		RACE_ASSERT(valid);
		return &data;
	}
	
	constexpr T const& operator*() const
	{
		return data;
	}
	
	T & operator*()
	{
		RACE_ASSERT(valid);
		return data;
	}
	
	constexpr explicit operator bool() const noexcept
	{
		return valid;
	}
	
	constexpr T const& value() const
	{
		return data;
	}
	
	T & value()
	{
		RACE_ASSERT(valid);
		return data;
	}
};

template <typename T>
inline constexpr optional<T> MakeOptional(T v)
{
	return optional<T>(v);
}

}// namespace racing

#endif
