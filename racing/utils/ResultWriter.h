//
//  ResultWriter.h
//  racing
//
//  Created by Jung on 4/18/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__ResultWriter__
#define __racing__ResultWriter__
#include <string>

#include "writer.h"
#include "stringbuffer.h"

namespace racing {
class ResultWriter
{
public:
    explicit ResultWriter();

    void SetTrack(std::string const& trackJson);

    void SendThrottle(float throttle);
    void RecvData(std::string const& jsonString);

    std::string GetJson();

private:
    rapidjson::StringBuffer buf_;
    rapidjson::Writer<rapidjson::StringBuffer> writer_;

    bool isWriting_;
    bool isStarted_;
    bool isInObject_;
};
}

#endif /* defined(__racing__ResultWriter__) */
