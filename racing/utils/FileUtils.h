//
//  FileUtils.h
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__FileUtils__
#define __racing__FileUtils__


#include <string>

namespace racing
{
class FileUtils
{
public:
    static std::string LoadFile(std::string const& filename);

    static bool SaveFile(std::string const& filename, std::string const& content);
};
}

#endif /* defined(__racing__FileUtils__) */
