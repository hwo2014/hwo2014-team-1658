//
//  ConfigUtils.cpp
//  racing
//
//  Created by mimu on 4/15/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "ConfigUtils.h"

#include <sstream>

namespace racing {

optional<GameConfig> ConfigUtils::ParseArgs(std::vector<std::string> const& args)
{
    if (args.size() < 4) {
        return optional<GameConfig>();
    }

    GameConfig config;

    config.host = args[1];
    config.port = args[2];

    config.botname = args[3];
    config.botkey = args[4];

    if (args.size() > 5) {
        config.trackName = args[5];
    }
    if (args.size() > 6) {
        config.password = args[6];
    }
    if (args.size() > 7) {
        config.testType = args[7];
    }
    if (args.size() > 8) {
        std::stringstream ss;
        ss << args[8];
        ss >> config.carCount;
    }

    return std::move(config);
}
}