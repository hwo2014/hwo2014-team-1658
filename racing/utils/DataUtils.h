//
//  DataUtils.h
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__DataUtils__
#define __racing__DataUtils__

#include "Datas.h"

namespace racing
{

class DataUtils
{
public:
    static std::string TypeString(DataTypes dataType);
    static DataTypes TypeFromString(std::string const& dataTypeString);

    static DataTypes TypeFromJson(std::string const& jsonString);
};

}

#endif /* defined(__racing__DataUtils__) */
