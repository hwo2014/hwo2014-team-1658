//
//  ResultWriter.cpp
//  racing
//
//  Created by Jung on 4/18/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "ResultWriter.h"

#include "document.h"

namespace racing {
ResultWriter::ResultWriter()
: writer_(buf_)
, isWriting_(false)
, isInObject_(false)
, isStarted_(false)
{
    isWriting_ = true;
    writer_.StartObject();
}

void ResultWriter::SetTrack(std::string const& trackJson)
{
    if (!isWriting_) {
        return;
    }
    if (isStarted_) {
        return;
    }

    rapidjson::Document doc;
    doc.Parse<0>(trackJson.c_str());

    if (doc.HasParseError()) {
        return;
    }

    writer_.String("track");
    doc.Accept(writer_);
}

void ResultWriter::SendThrottle(float throttle)
{
    if (!isWriting_) {
        return;
    }
    if (!isStarted_) {
        writer_.String("race");
        writer_.StartArray();
        isStarted_ = true;
    }

    if (isInObject_) {
        writer_.EndObject();
        isInObject_ = false;
    }

    writer_.StartObject();

    writer_.String("throttle");
    writer_.Double(throttle);

    isInObject_ = true;
}

void ResultWriter::RecvData(std::string const& jsonString)
{
    if (!isWriting_) {
        return;
    }

    if (!isInObject_) {
        return;
    }

    rapidjson::Document doc;
    doc.Parse<0>(jsonString.c_str());

    if (doc.HasParseError()) {
        return;
    }

    writer_.String("data");
    doc.Accept(writer_);
    writer_.EndObject();

    isInObject_ = false;
}

std::string ResultWriter::GetJson()
{
    isWriting_ = false;

    if (isInObject_) {
        writer_.EndObject();
    }

    if (isStarted_) {
        writer_.EndArray();
    }
    writer_.EndObject();

    return std::move(buf_.GetString());
}
} // racing