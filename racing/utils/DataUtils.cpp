//
//  DataUtils.cpp
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "DataUtils.h"
#include "document.h"
#include <map>

namespace racing
{
static const std::map<DataTypes, std::string> DataTypeStrings {
    {DataTypes::Invalid, "invalid"},
    {DataTypes::Error, "error"},
    {DataTypes::Join, "join"},
    {DataTypes::GameInit, "gameInit"},
    {DataTypes::YourCar, "yourCar"},
    {DataTypes::CarPosition, "carPositions"},
    {DataTypes::GameStart, "gameStart"},
    {DataTypes::Throttle, "throttle"},
    {DataTypes::GameEnd, "gameEnd"},
    {DataTypes::TournamentEnd, "tournamentEnd"},
    {DataTypes::Crash, "crash"},
    {DataTypes::Spawn, "spawn"},
    {DataTypes::LapFinished, "lapFinished"},

    {DataTypes::DNF, "dnf"},
    {DataTypes::Finish, "finish"},
    {DataTypes::SwitchLane, "switchLane"},

    {DataTypes::CreateRace, "createRace"},
    {DataTypes::JoinRace, "joinRace"},
    {DataTypes::Ping, "ping"},
};
//------------------------------------------------------------------------------
std::string DataUtils::TypeString(DataTypes dataType)
{
    return DataTypeStrings.at(dataType);
}
//------------------------------------------------------------------------------
DataTypes DataUtils::TypeFromString(std::string const& dataTypeString)
{
    for (auto const& kv : DataTypeStrings) {
        if (kv.second == dataTypeString) {
            return kv.first;
        }
    }

    return DataTypes::Invalid;
}
//------------------------------------------------------------------------------
DataTypes DataUtils::TypeFromJson(std::string const& jsonString)
{
    using namespace rapidjson;

    Document doc;
    doc.Parse<0>(jsonString.c_str());

    if (doc.HasParseError() || !doc.IsObject()
        || !doc.HasMember("msgType") || !doc["msgType"].IsString()) {
        return DataTypes::Invalid;
    }

    return TypeFromString(doc["msgType"].GetString());
}
//------------------------------------------------------------------------------
}