//
//  Assert.h
//  racing
//
//  Created by mimu on 13/09/04.
//
//

#ifndef racing_Assert_h
#define racing_Assert_h

#include <cassert>

namespace racing {
namespace Details {

#if defined(DEBUG) && defined(__APPLE_CC__)
#	// Debug Mode under Apple Xcode
#	define RACE_ASSERT(expression) RACE_ASSERT_MESSAGE(expression, "RACE_ASSERT")
#	define RACE_ASSERT_MESSAGE(expression, message) \
		do {\
			if (!(expression)) {\
				assert(message && expression);\
			}\
		} while(false)
#elif defined(DEBUG)
#	// Debug Mode
#	define RACE_ASSERT(expression) RACE_ASSERT_MESSAGE(expression, "RACE_ASSERT")
#	define RACE_ASSERT_MESSAGE(expression, message) \
		do {\
			if (!(expression)) {\
				assert(message && expression);\
			}\
		} while(false)
#else //!defined(DEBUG) || defined(NDEBUG)
#	// Release mode
#	define RACE_ASSERT(expression)
#	define RACE_ASSERT_MESSAGE(expression, message)
#endif

}// namespace Details
}// namespace racing

#endif
