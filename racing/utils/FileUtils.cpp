//
//  FileUtils.cpp
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "FileUtils.h"

#include <iostream>
#include <fstream>

namespace racing
{
//------------------------------------------------------------------------------
std::string FileUtils::LoadFile(std::string const& filename)
{
    std::string output;
    std::ifstream ifs(filename, std::ios::in);

    if (!ifs.is_open()) {
        return std::move(output);
    }

    std::string line;
    while (std::getline(ifs, line)) {
        output.append(line);
    }

    ifs.close();

    return std::move(output);
}
//------------------------------------------------------------------------------
bool FileUtils::SaveFile(std::string const& filename, std::string const& content)
{
    std::ofstream ofs(filename, std::ios::out);
    if (!ofs.is_open()) {
        return false;
    }

    ofs << content;

    ofs.close();

    return false;
}
//------------------------------------------------------------------------------
}