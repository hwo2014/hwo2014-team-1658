//
//  ConfigUtils.h
//  racing
//
//  Created by mimu on 4/15/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__ConfigUtils__
#define __racing__ConfigUtils__

#include <string>
#include <vector>
#include "Optional.h"

namespace racing {

struct GameConfig
{
    GameConfig()
    : carCount(1)
    {}
    
    std::string host;
    std::string port;

    std::string botname;
    std::string botkey;

    std::string trackName;
    std::string password;

    std::string testType;

    std::uint16_t carCount;
};

class ConfigUtils
{
public:
    static optional<GameConfig> ParseArgs(std::vector<std::string> const& args);
};

}

#endif /* defined(__racing__ConfigUtils__) */
