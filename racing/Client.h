//
//  Client.h
//  racing
//
//  Created by mimu on 4/15/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__Client__
#define __racing__Client__

#include <boost/asio.hpp>
#include <deque>
#include <functional>
#include <memory>

#include "ConfigUtils.h"
#include "GameLogic.h"

namespace racing {

class Client
{
public:
    explicit Client(boost::asio::io_service& io_service,
                    boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
                    GameConfig const& config);

    void Write(std::string && msg);
    void Close();
//    void ConnectCallbacks(std::function<void()> const& onConnect,
//                          std::function<void(std::string const&&)> const& onRead);

private:
    void DoConnect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator);
    void DoRead();
    void DoWrite();

    void OnRead(std::string const&& msg);

private:
    GameLogic gameLogic_;

    boost::asio::streambuf buffer_;
    std::deque<std::string> sendQueue_;
    boost::asio::io_service& io_service_;
    boost::asio::ip::tcp::socket socket_;

    std::function<void()> onConnect_;
    std::function<void(std::string const&&)> onRead_;
};
}

#endif /* defined(__racing__Client__) */
