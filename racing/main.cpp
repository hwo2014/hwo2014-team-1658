//
//  main.cpp
//  racing
//
//  Created by mimu on 4/14/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include <iostream>
#include <boost/asio.hpp>
#include <thread>

#include "Client.h"
#include "Datas.h"
#include "DataUtils.h"
#include "ConfigUtils.h"

#include <functional>

using boost::asio::ip::tcp;
using namespace racing;

int main(int argc, const char * argv[])
{
    try
    {
        if (argc < 5)
        {
            std::cerr << "Usage: racing <host> <port> <botname> <botkey> "
                "<track:optional> <password:optional> <join|create:optional> <car_count:optional on create>\n";
            return 1;
        }

        std::vector<std::string> args;
        for (auto i=0; i<argc; ++i) {
            args.push_back(argv[i]);
        }

        auto config = ConfigUtils::ParseArgs(args);

        boost::asio::io_service io_service;
        tcp::resolver resolver(io_service);

        auto endpoint_iterator = resolver.resolve({config->host, config->port});

        Client client(io_service, endpoint_iterator, *config);

        std::thread t([&io_service](){ io_service.run(); });
        t.join();

        client.Close();
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}

