//
//  GameLogic.cpp
//  racing
//
//  Created by mimu on 4/15/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "GameLogic.h"
#include "DataUtils.h"
#include "Algorithm.h"

#include <iostream>
#include "FileUtils.h"

namespace racing {
//------------------------------------------------------------------------------
GameLogic::GameLogic(GameConfig const& config, std::function<void(std::string && msg)> writeFunc)
: config_(config)
, writeFunc_(writeFunc)
, gameStarted_(false)
{
}
//------------------------------------------------------------------------------
void GameLogic::OnConnect()
{
    if (!config_.testType.empty()) {
        ///@note test
        if (config_.testType == "create") {
            // create
            CreateRaceData data;
            data.name = config_.botname;
            data.key = config_.botkey;
            data.trackName = config_.trackName;
            data.password = config_.password;
            data.carCount = config_.carCount;

            Send(std::move(data.ToJson()));
        } else {
            // join
            JoinRaceData data;
            data.name = config_.botname;
            data.key = config_.botkey;
            data.trackName = config_.trackName;
            data.password = config_.password;

            Send(std::move(data.ToJson()));
        }
    } else {
        ///@note production

        JoinRaceData data;
        data.name = config_.botname;
        data.key = config_.botkey;

        if (!config_.trackName.empty() && !config_.password.empty()) {
            data.trackName = config_.trackName;
            data.password = config_.password;
        }

        Send(std::move(data.ToJson()));
    }

}
//------------------------------------------------------------------------------
void GameLogic::OnRead(std::string const&& msg)
{
    printf("%s\n\n", msg.c_str());

    auto dataType = DataUtils::TypeFromJson(msg);

    if (dataType == DataTypes::Join) {
        JoinData data;
        if (data.FromJson(msg)) {
            OnJoin(std::move(data));
        } else {
            OnError(std::move(msg));
        }
    }
    else if (dataType == DataTypes::CreateRace) {

    }
    else if (dataType == DataTypes::YourCar) {
        YourCarData data;
        if (data.FromJson(msg)) {
            OnYourCar(std::move(data));

        } else {
            OnError(std::move(msg));
        }
    }
    else if (dataType == DataTypes::GameInit) {
        GameInitData data;
        if (data.FromJson(msg)) {

            writer_.SetTrack(msg);

            OnGameInit(std::move(data));

        } else {
            OnError(std::move(msg));
        }
    }
    else if (dataType == DataTypes::GameStart) {
        OnGameStart();
    }
    else if (dataType == DataTypes::CarPosition) {
        CarPositionData data;
        if (data.FromJson(msg)) {

            writer_.RecvData(msg);
            OnCarPositions(std::move(data));

        } else {
            OnError(std::move(msg));
        }
    }
    else if (dataType == DataTypes::Crash) {
        CrashData data;
        if (data.FromJson(msg)) {

            FileUtils::SaveFile("/tmp/racing.json", writer_.GetJson());

            OnCrash(std::move(data));

        } else {
            OnError(std::move(msg));
        }
    }
    else if (dataType == DataTypes::GameEnd) {
        GameEndData data;
        if (data.FromJson(msg)) {
            OnGameEnd(std::move(data));

        } else {
            OnError(std::move(msg));
        }
    }
    else if (dataType == DataTypes::LapFinished) {
        ///
    }
    else if (dataType == DataTypes::Spawn) {
        SpawnData data;
        if (data.FromJson(msg)) {
            OnSpawn(std::move(data));

        } else {
            OnError(std::move(msg));
        }
    }
    else {
        std::cout << "Unknown message type : " << DataUtils::TypeString(dataType) << std::endl;

        OnError(std::move(msg));
    }
    
}
//------------------------------------------------------------------------------
void GameLogic::Send(std::string && msg)
{
    writeFunc_(std::move(msg));
}
//------------------------------------------------------------------------------
#pragma mark - On Data
//------------------------------------------------------------------------------
void GameLogic::OnJoin(JoinData const&& data)
{

}
//------------------------------------------------------------------------------
void GameLogic::OnYourCar(YourCarData const&& data)
{
    myCarData_.name = data.name;
    myCarData_.color = data.color;
}
//------------------------------------------------------------------------------
void GameLogic::OnGameInit(GameInitData const&& data)
{
    trackData_ = std::move(data);
}
//------------------------------------------------------------------------------
void GameLogic::OnGameStart()
{
    gameStarted_ = true;
    ThrottleData tData;
    tData.data = 1.0f;

    writer_.SendThrottle(1.0f);

    Send(tData.ToJson());
}
//------------------------------------------------------------------------------
void GameLogic::OnCarPositions(CarPositionData const&& data)
{
    if (!gameStarted_) {
        return;
    }

    CarPosition position;
    for (auto const& p : data.positions) {
        if (p.name == myCarData_.name) {
            position = p;
        }
    }

    if (data.gameTick > 0) {
        auto speed = Algorithm::CalculateSpeed(trackData_.race.track, lastPos_, position);
        printf("speed : %f\n", speed);
    }

    Race::Track::Piece currentPiece = trackData_.race.track.pieces[position.pieceIndex];

    auto next_index = position.pieceIndex + 1;

    if (position.pieceIndex == trackData_.race.track.pieces.size() -1) {
        // last piece
        next_index = 0;
    }
    Race::Track::Piece nextPiece = trackData_.race.track.pieces[next_index];

    float throttle = 0.0f;
//    if (static_cast<std::uint32_t>(currentPiece.angle) % 90 == 0 && static_cast<std::uint32_t>(nextPiece.angle) % 90 == 0) {
//        throttle = 1.0f;
//    } else if (static_cast<std::uint32_t>(currentPiece.angle) % 90 == 0 && static_cast<std::uint32_t>(nextPiece.angle) % 90 != 0) {
//        throttle = 0.5f;
//    } else {
//        throttle = 0.6f;
//    }

    throttle = 0.7f;
    writer_.SendThrottle(throttle);

    lastPos_ = std::move(position);

    ThrottleData tData;
    tData.data = throttle;

    Send(tData.ToJson());
}
//------------------------------------------------------------------------------
void GameLogic::OnCrash(CrashData const&& data)
{

}
//------------------------------------------------------------------------------
void GameLogic::OnSpawn(SpawnData const&& data)
{
    ThrottleData tData;
    tData.data = 1.0f;
    Send(tData.ToJson());
}
//------------------------------------------------------------------------------
void GameLogic::OnGameEnd(GameEndData const&& data)
{
    FileUtils::SaveFile("/tmp/racing.json", writer_.GetJson());
}
//------------------------------------------------------------------------------
void GameLogic::OnError(std::string const&& msg)
{
    std::cout << "onError" << std::endl;
    Send("{\"msgType\":\"ping\",data:null}\n");
}
//------------------------------------------------------------------------------
}