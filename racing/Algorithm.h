//
//  Algorithm.h
//  racing
//
//  Created by mimu on 4/16/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__Algorithm__
#define __racing__Algorithm__

#include "Datas.h"

namespace racing {
class Algorithm
{
public:
    static float CalculateSpeed(Race::Track const& track, CarPosition const& lastPos, CarPosition const& currentPos);
};
}

#endif /* defined(__racing__Algorithm__) */
