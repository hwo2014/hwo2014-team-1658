//
//  Algorithm.cpp
//  racing
//
//  Created by mimu on 4/16/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#include "Algorithm.h"
#include <cmath>

constexpr float RADIAN = 0.0174532925;

namespace racing {
float Algorithm::CalculateSpeed(Race::Track const& track, CarPosition const& lastPos, CarPosition const& currentPos)
{
    if (lastPos.pieceIndex == currentPos.pieceIndex) {
        return currentPos.inPieceDistance - lastPos.inPieceDistance;
    }

    auto const& lastPiece = track.pieces[lastPos.pieceIndex];

    float length = lastPiece.length;

    if (lastPiece.radius != 0) {
        float laneOffset = 0.0f;
        auto const& lastLane = track.lanes[lastPos.endLaneIndex];

        ///@todo need to check lane is inside or outside
        if (lastPiece.angle > 0) {
            /// bend right
            if (lastLane.distanceFromCenter < 0) {
                /// outside
                laneOffset = std::abs(lastLane.distanceFromCenter);
            } else {
                /// inside
                laneOffset = -1 * std::abs(lastLane.distanceFromCenter);
            }
        } else {
            /// bend left
            if (lastLane.distanceFromCenter < 0) {
                /// inside
                laneOffset = -1 * std::abs(lastLane.distanceFromCenter);
            } else {
                /// outside
                laneOffset = std::abs(lastLane.distanceFromCenter);
            }
        }

        length = (lastPiece.radius + laneOffset)* RADIAN * std::abs(lastPiece.angle);
    }

    auto speed = currentPos.inPieceDistance + (length - lastPos.inPieceDistance);

    return speed;
}
}