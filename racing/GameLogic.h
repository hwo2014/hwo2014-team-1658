//
//  GameLogic.h
//  racing
//
//  Created by mimu on 4/15/14.
//  Copyright (c) 2014 mimu. All rights reserved.
//

#ifndef __racing__GameLogic__
#define __racing__GameLogic__

#include <functional>
#include <string>

#include "ConfigUtils.h"
#include "Datas.h"
#include "ResultWriter.h"

namespace racing {
class GameLogic
{
public:
    explicit GameLogic(GameConfig const& config,
                       std::function<void(std::string && msg)> writeFunc);

    void OnConnect();
    void OnRead(std::string const&& msg);

private:
    void OnJoin(JoinData const&& data);
    void OnYourCar(YourCarData const&& data);
    void OnGameInit(GameInitData const&& data);
    void OnGameStart();
    void OnCarPositions(CarPositionData const&& data);
    void OnCrash(CrashData const&& data);
    void OnSpawn(SpawnData const&& data);
    void OnGameEnd(GameEndData const&& data);
    void OnError(std::string const&& msg);

    void Send(std::string && msg);

private:
    GameConfig config_;
    bool gameStarted_;

    GameInitData trackData_;
    Car myCarData_;

    CarPosition lastPos_;

    ResultWriter writer_;

    std::function<void(std::string && msg)> writeFunc_;
};
}

#endif /* defined(__racing__GameLogic__) */
